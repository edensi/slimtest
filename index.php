<?php
require "bootstrap.php";
require "vendor/autoload.php";
use Chatter\Models\Product;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging());

$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
$app->get('/products', function($request, $response,$args){
    $_product = new Product();
    $products = $_product->all();

    $payload = [];
    foreach($products as $u){
        $payload[$u->id] = [
            "name" => $u->name,
            "price" => $u->price
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->put('/products/{product_id}', function($request, $response, $args){
    $name = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');
    $_product = Product::find($args['product_id']);
    $_product->name = $name;
    $_product->price = $price;
   
    if($_product->save()){
        $payload = ['product_id' => $_product->id,"result" => "The product has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->get('/products/{product_id}', function($request, $response,$args){
    $_id = $args['product_id'];
    $product = Product::find($_id);
    return $response->withStatus(200)->withJson($product);
});

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();